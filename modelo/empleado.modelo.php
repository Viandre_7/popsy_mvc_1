<?php
namespace modelo;
use PDO;
use Exception;
include_once("../entorno/conexion.php");
require_once "session.php";

class Empleado{
    private $identificacion;
    private $name;
    private $apellido;
    private $nick;
    private $password;
    //
    private $retorno;
    private $conexion;
    private $sql="";

    public function __construct(\entidad\Empleado $empleadoE){
        $this->Id     = $empleadoE->getIdEmpl();
        $this->Ident  = $empleadoE->getIdent();
        $this->Name   = $empleadoE->getName();
        $this->Apell  = $empleadoE->getApell();
        $this->Nick   = $empleadoE->getNick();
        $this->Pass   = $empleadoE->getPass();

        $this->session = new \Session(); //instanciamos la clase session para poder usar sus valores
        $this->conexion = new \Conexion();
    }
    public function create(){
        try { // creamos un try catch para capturar los errores que pueden ocurrir al trabajar las consultas de la base de datos
            $this->sql = "INSERT INTO `empleado` ( `identificacion`, `nombre`, `apellido`, `usuario`, `estado`, `clave`) VALUES ( '$this->Ident', '$this->Name', '$this->Apell', '$this->Nick', 'activo', '$this->Pass');";
            $this->result = $this->conexion->conn->query($this->sql);
            $this->retorno = "Se registro el nuevo empleado";
        }catch (Exception $e) {
            $this->retorno = $e->getMessage();
        }
        return $this->retorno;
    }
    public function update() {//actualizamos los datos que queremos del usuario
        try {
            $this->sql = "UPDATE empleado SET usuario='$this->Nick' , clave='$this->Pass' WHERE id_empleado=$this->Id";
            echo $this->sql;
            $this->result = $this->conexion->conn->query($this->sql);
            $this->retorno = "Se modifico el empleado";
        }catch (Exception $e) {
            $this->retorno = $e->getMessage();
        }
        return $this->retorno;
    }
    public function delete() {
        try {
            /* se actualiza por que el usuario puede tener una relacion con otras tablas y esto me
            daria un error, primero se deben eliminar los registros con los que esta relacionado
            para luego si proecder a eliminar, como estos registros son de compras son datos que no
            se eliminan por tanto el usuario solo se actualiza para tener un historico de la data y
            por si quiere volver a trabajar */
            $this->sql = "UPDATE empleado SET estado='inactivo' WHERE id_empleado=$this->Id";
            $this->result = $this->conexion->conn->query($this->sql);
            $this->retorno = "Se elimino la categoria";
        }catch (Exception $e) {
            $this->retorno = $e->getMessage();
        }
        return $this->retorno;
    }
    public function read(){
        try {
            $this->sql = "SELECT id_empleado,identificacion,nombre,usuario,clave FROM empleado where estado='activo';";
            $this->result = $this->conexion->conn->query($this->sql);
            $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC); //aqui convertimos el resultado de la consulta en un objeto de tipo arreglo que podremos utilizar facilmente.
        }catch (Exception $e) {
            $this->retorno = $e->getMessage();
        }
        return $this->retorno;
    }
            // se inicia la funcion login para iniciar seccion se inicia dos valores el usuario y la contraseña 
            // con el id que resulta de consultar esos parametros ,se hacen unas condiciones 
    public function login(){  
        try {
            $this->sql = "SELECT id_empleado FROM empleado where estado='activo' AND usuario='$this->Nick ' AND clave='$this->Pass';";
            $this->result = $this->conexion->conn->query($this->sql);
            $back = $this->result->fetchAll(PDO::FETCH_ASSOC);
        }catch (Exception $e) {
            $this->retorno = $e->getMessage();
        }
        if (empty($back)) {  // empty si el resultado de la consulta es vacio o lleno -si esta vacio retorna no 
            return 'no';
        }else{  // si existe un usuario activo llama la clase sesion  y da inicio llamando la funcion init que inicializa la  funcion del usuario 
            $this->session->init();
            $this->session->set("id", $back[0]['id_empleado']);
            return 'ok';
        }

    }
}
?>