<?php
    class Session
    {
        function __construct(){
        }

        function init()
        {
            @session_start();
        }
        //creamos una funcion en la cual crearemos las variables de sesion
        //esta necesitara de un nombre de la variable y un valor de la variable de sesion que se consultan en las vistas 
        function set($name, $valor)
        {
            $_SESSION[$name]= $valor;
        }

        function destroy()
        {
            session_destroy();
            session_unset();
        }
        //con esta adquirimos las variables de sesion que utilizaremos
        function get($variable){
            return $_SESSION[$variable];
        }
    }
 ?>