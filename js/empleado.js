//fetch es una variable nueva de javascript que funciona para hacer  los metodos get put post delete update...
$(document).ready(()=>{
    buscar();//funcion para pintar la tabla de usuarios
    $(document).on("click","#btnRegistrar",()=>{
        let a = $("#identificacion").val();//captura el valor del imput que contiene este id
        let b = $("#name").val();
        let c = $("#apellido").val();
        let d = $("#nick").val();
        let e = $("#password").val();
        let f = $("#password2").val();

        if (a==""||b=="" ||c=="" ||d=="" ||e=="" ||f=="") {
            return alert("Se deben llenar todos los campos")
        }else if (e!=f) { //si las contraseñas son diferentes muestro la alerta
            return alert("Las contraseñas no coinciden")
        }
        $.ajax({
            url:"../controlador/empleado.create.php",
            type:"POST",
            datatype:"JSON",
            data:$("#empleadoFrm").serialize()
        }).done((json)=>{
            console.log(json.trim());
            limpiarCampos();
            buscar();
        }).fail((xhr,status,error)=>{
            console.log(error);
        })
    });

    $(document).on("click","#updateModal",()=>{
        $.ajax({
            url:"../controlador/empleado.update.php",
            type:"POST",
            datatype:"JSON",
            data:$("#empleadoFrm").serialize()
        }).done((json)=>{
            console.log(json);
            buscar();
        }).fail((xhr,status,error)=>{
            console.log(error);
        })
    });

    $(document).on("click","#deleteModal",()=>{
        $.ajax({
            url:"../controlador/empleado.delete.php",
            type:"POST",
            datatype:"JSON",
            data:$("#empleadoFrm").serialize()
        }).done((json)=>{
            console.log(json.trim());
            buscar();
        }).fail((xhr,status,error)=>{
            console.log(error);
        })
    });
    function limpiarCampos() {
        $("#identificacion").val("");
        $("#name").val("");
        $("#apellido").val("");
        $("#nick").val("");
        $("#password").val("");
        $("#password2").val("");
    }

    function buscar() {
        $.ajax({
            url:"../controlador/empleado.read.php",
            type:"POST",
            datatype:"JSON",
            data:null
        }).done((json)=>{
            //console.log(json);
            try {
                crearMatriz(JSON.parse(json));//a la matriz le pasamos un objeto del controlador empleado
                $('#myTable').DataTable({
                    "language": {
                        "url": "../js/Spanish.json",
                        "buttons":{
                            copyTitle: "Registro(s) Copiado(s)",
                            copySuccess:{
                                _:'%d Registros Copiados',
                                1:'%d Registros Copiado'
                            }
                        }
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fas fa-file-pdf"></i>',
                            download: 'open',
                            titleAttr:"PDF",
                            title: 'Reporte de empleados',
                            exportOptions: {
                                columns:[0,1]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fas fa-file-excel"></i>',
                            autoFilter: true,
                            titleAttr:"Excel",
                            title: 'Reporte de empleados',
                            exportOptions: {
                                columns:[0,1]
                            }
                        },
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fas fa-copy"></i>',
                            titleAttr:"Copiar",
                            title: 'Reporte de empleados',
                            exportOptions: {
                                columns:[0,1]
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fas fa-print"></i>',
                            titleAttr:"Imprimir",
                            title: 'Reporte de empleados',
                            exportOptions: {
                                columns:[0,1]
                            }
                        }
                ]
                });
            } catch (e) {
                console.log(e)
            }
        }).fail((xhr,status,error)=>{
            console.log(error);
        })
    }
});

function crearMatriz(x) {//funcion para pintar los datos en el html

    let datos = "<table id='myTable' class='table table-dark text-center' border=3><thead><tr><td>Nombre</td><td>Identificacion</td><td>usuario</td><td>contraseña</td><td>Modificar</td><td>Eliminar</td></tr></thead>"
    datos += "<tbody>"
    $.each(x,(key,value)=>{ //recorremos el objeto con un forech de jquery
        datos +=`<tr class='bgTable'><td>${value.nombre}</td>`;
        datos +=`<td>${value.identificacion}</td>`;
        datos +=`<td>${value.usuario}</td>`;
        datos +=`<td>******</td>`;
        datos += `<td><a class="btn btn-info" onclick="modal('update');accion(${value.id_empleado},'${value.usuario}','${value.clave}');" data-toggle="modal" data-target="#modal"><i class="far fa-edit"></i></a></td>`;
        datos += `<td><a class="btn btn-danger" onclick="modal('delete');accion(${value.id_empleado},'${value.usuario}','${value.clave}');" data-toggle="modal" data-target="#modal"><i class="far fa-trash-alt"></i></a></td></tr>`;
    });
    datos += `</tbody></table>`;
    $("#respuesta").html(datos);
}
function modal(x){
    let j = "";
    if (x=="update"){
        $("#exampleModalLongTitle").html("Modificar empleado");
        j = '<table class="table"><tbody>';
        j+= '<tr hidden><td>Id:</td><td><center><input class="form-control" id="idm" type="text" name="idm"></center></td></tr>';
        j+='<tr><td>usuario:</td><td><center><input class="form-control" id="nickm" type="text" name="nickm"></center></td></tr>';
        j += '<tr><td>contraseña:</td><td><center><input class="form-control" id="passwordm" type="text" name="passwordm"></center></td></tr></tbody></table>';
        $(".modal-body").html(j);
        $(".modalConfirmacion").html("Modificar");
        $(".modalConfirmacion").prop("id","updateModal");
    }else{
        $("#exampleModalLongTitle").html("Desea continuar?");
        j = '<table class="table"><tbody>';
        j+= '<tr hidden><td>Id:</td><td><center><input class="form-control" id="idm" type="text" name="idm"></center></td></tr></tbody></table>';
        $(".modal-body").html(j);
        $(".modalConfirmacion").html("Eliminar");
        $(".modalConfirmacion").prop("id","deleteModal");
    }
}
function accion(a,b,c) {
    $("#idm").val(a);
    $("#nickm").val(b);
    $("#passwordm").val(c);
}
