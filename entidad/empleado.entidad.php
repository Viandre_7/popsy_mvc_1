<?php
namespace entidad;
class Empleado{

    private $idEmpl;
    private $idEmpleado;
    private $identificacion;
    private $name;
    private $apellido;
    private $nick;
    private $password;
    private $Ident;
    private $Pass;
    private $Apell;

    public function getIdEmpl(){
        return $this->idEmpl;
    }
    public function setIdEmpl($idEmpleado){
        $this->idEmpl = $idEmpleado;
    }

    public function getIdent(){
        return $this->Ident;
    }
    public function setIdent($identificacion){
        $this->Ident = $identificacion;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getApell(){
        return $this->Apell;
    }
    public function setApell($apellido){
        $this->Apell = $apellido;
    }
    public function getNick(){
        return $this->nick;
    }
    public function setNick($nick){
        $this->nick = $nick;
    }

    public function getPass(){
        return $this->Pass;
    }
    public function setPass($password){
        $this->Pass = $password;
    }
}

?>