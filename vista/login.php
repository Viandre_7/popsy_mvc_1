<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/all.css">
    <link rel="stylesheet" href="../css/datatables.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="icon" href="../img/popsy.png	">
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/all.js"></script>
    <script src="../js/datatables.js"></script>
    <script src="../js/dataTables.buttons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/signin.css">
</head>

<body class="text-center">

<form class="form-signin" id="loginFrm">
  <img class="mb-4" src="../img/popsy.png" alt="" width="72" height="72">
  <h1 class="h3 mb-3 font-weight-normal">Por favor, regístrese.</h1>
  <label for="inputUser" class="sr-only">Usuario del Sistema</label>
  <input type="text" id="inputUser"  name="inputUser" class="form-control" placeholder="Usuario" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" id="inputPassword"  name="inputPassword" class="form-control" placeholder="Contraseña" required>
  <br>
  <div class="btn btn-lg btn-primary btn-block" id="btnRegistrar">Ingresar</div>
  <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
</form>

</body>
<script src="../js/login.js"></script>
</html>

