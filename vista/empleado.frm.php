<?php
include_once("header.php")   ?>
    <title>Document</title>
</head>
<body>
    <?php require_once('menu.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <form id="empleadoFrm">
                    <div class="row form-group">
                        <div class="col-12 mb-3 mt-5">
                            <h1 class="text-center">Ingresar empleados en el Sistema</h1>
                        </div>
                        <div class="col-12">
                            <label>identificacion</label>
                            <input class="form-control" id="identificacion" type="text" name="identificacion">
                        </div>
                        <div class="col-12">
                            <label>Nombre de usaurio</label>
                            <input class="form-control" id="name" type="text" name="name">
                        </div>
                        <div class="col-12">
                            <label>Apellido</label>
                            <input class="form-control" id="apellido" type="text" name="apellido">
                        </div>
                        <div class="col-12">
                            <label>usuario(nick)</label>
                            <input class="form-control" id="nick" type="text" name="nick">
                        </div>
                        <div class="col-12">
                            <label>contraseña</label>
                            <input class="form-control" id="password" type="password" name="password">
                        </div>
                        <div class="col-12">
                            <label>repetir contraseña</label>
                            <input class="form-control" id="password2" type="password" name="password2">
                        </div>
                        <div class="col-12 mt-4" style="margin-bottom: 2em;">
                            <a class="btn btn-primary mr-3" id="btnRegistrar">Registrar</a>
                        </div>
                    </div>
                    <div class="content">
                      <h1>Empleados en el sistema</h1><br>
                    </div>
                    <div class="row justify-content-center" id="respuesta">
                    </div>

                    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary modalConfirmacion" data-dismiss="modal"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
    <script src="../js/empleado.js"></script>
</body>
</html>

