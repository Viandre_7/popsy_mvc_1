<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="#">Popsy</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="venta.frm.php">Venta <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ingrediente.frm.php">Ingredientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categoria.frm.php">categoria</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="sabor.frm.php">sabor</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="empleado.frm.php">Nuevo empleado</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../controlador/sessiond.php">Cerrar Sesion</a>
      </li>

    </ul>
  </div>
</nav>


