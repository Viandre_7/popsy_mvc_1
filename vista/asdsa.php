    <?php include_once("header.php")           ?>
    <title>Document</title>
</head>
<body>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div#file{
            position:relative;
            margin: 0;
            padding: 10px;
            width: 150px;
            background-size: cover; 
            background-repeat: no-repeat;
            background-position: center;
        }
        div>input#imagen{
            position: absolute;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            width: 100%;
            height: 100%;
            opacity: 0;
        }
        div>p#texto{
            text-align: center;
            color: transparent;
        }
    </style>
    <div id="file">
        <p id="texto">a</p>
        <input type="file" name="imagen" id="imagen" onchange="imagenServidor()">
    </div>
    <script>
        //$("#imagen").val()
        $("#file").css('background-image', 'url("../img/Nuevos/asd.jpeg")');
        function imagenServidor() {
            /* let x = $("#imagen").val();
            let img = $("#imagenServidor").prop("src",x)
            console.log(x) */
            let input = document.getElementById("imagen");
            let fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            fReader.onloadend = function(event){
                let img = document.getElementById("file");
                console.log(event.target.result)
                $("#file").css('background-image', `url(${event.target.result})`);
            }
        }
        
    </script>
        
    
</body>
</html>