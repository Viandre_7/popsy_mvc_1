<?php
include_once("../entidad/empleado.entidad.php");
include_once("../modelo/empleado.modelo.php");

$id=$_POST['idm'];

$empleadoE = new entidad\empleado();
$empleadoE->setIdEmpl($id);

$empleadoM = new modelo\empleado($empleadoE);
$retorno = $empleadoM->delete();

unset($empleadoE);
unset($empleadoM);

echo json_encode($retorno);

?>