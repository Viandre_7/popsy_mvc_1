<?php
include_once("../entidad/empleado.entidad.php");
include_once("../modelo/empleado.modelo.php");

$id=$_POST['idm'];
$usuario=$_POST['nickm'];
$clave=$_POST['passwordm'];

$empleadoE = new entidad\Empleado();
$empleadoE->setIdEmpl($id);
$empleadoE->setNick($usuario);
$empleadoE->setPass($clave);

$empleadoM = new modelo\Empleado($empleadoE);
$retorno = $empleadoM->update();

unset($empleadoE);
unset($empleadoM);

echo json_encode($retorno);

?>