<?php
include_once("../entidad/empleado.entidad.php");
include_once("../modelo/empleado.modelo.php");

$empleadoE = new entidad\Empleado();
$empleadoM = new modelo\Empleado($empleadoE);
$retorno = $empleadoM->read();
unset($empleadoE);
unset($empleadoM);

echo json_encode($retorno);
?>