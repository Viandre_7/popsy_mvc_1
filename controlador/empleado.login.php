<?php
include_once("../entidad/empleado.entidad.php");
include_once("../modelo/empleado.modelo.php");

$usuario=$_POST['inputUser'];
$clave=$_POST['inputPassword'];

$empleadoE = new entidad\Empleado();
$empleadoE->setNick($usuario);
$empleadoE->setPass($clave);

$empleadoM = new modelo\Empleado($empleadoE);
$retorno = $empleadoM->login();

unset($empleadoE);
unset($empleadoM);

echo json_encode($retorno);
?>