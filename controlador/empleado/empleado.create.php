<?php
include_once("../entidad/empleado.entidad.php");
include_once("../modelo/empleado.modelo.php");

$identificacion= $_POST['identificacion'];
$nombre        = $_POST['name'];
$apellido      = $_POST['apellido'];
$nick          = $_POST['nick'];
$contraseña    = $_POST['password'];

$empleadoE = new entidad\Empleado();
$empleadoE->setIdent($identificacion);
$empleadoE->setName($nombre);
$empleadoE->setApell($apellido);
$empleadoE->setNick($nick);
$empleadoE->setPass($contraseña);


$empleadoM = new modelo\Empleado($empleadoE);
$retorno = $empleadoM->create();

unset($empleadoE);
unset($empleadoM);

echo json_encode($retorno);

?>